/*
 * Dany Turcios
 * 10/26/2015
 * Math Operation Program
 * This program will calculate the factors of a number, determine if a number is prime or not
 * calculate the factorial of a number, calculate Fibonacci sequence and determine if a number is evenly
 * divisible by another number
 */
import java.util.Scanner;
public class MathOperations {
	//MAIN METHOD
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		//declare any variables and strings before the loop
		int choice, n, primeAnswer, d;
		//Assigning strings to have less clutter
		// menu1 is first blank before method
		String menu1=""; 
		//Strings used to remove clutter
		String farewell = "Bye.";
		String error = "Uh oh! The value you put in was not valid. Please try again.\n";
		//Menu loop. Use a do-while loop so the program will run one time, beginning the loop.
			do{
			//Call to Menu Method
			menu(menu1);
				//Response to menu
				choice=keyboard.nextInt();
				//Switch Statements for the menu choices
					switch(choice){
						case 1:
							System.out.println("Input a number greater than or equal to 2:");
							n = keyboard.nextInt();
								//If statement to restrict number from being less than 2
								if (n >= 2){
									getFactor(n);
							}
								//This will return you to the menu
								else{
								System.out.println(error);
								}
							System.out.println(" ");
								break;
						case 2:
							System.out.println("Input a number greater than or equal to 2:");
							n = keyboard.nextInt();
							//If statement to restrict number from being less than 2
								if (n >= 2){
									//call to function
									primeAnswer= prime(n);
									//Determines if prime or not
									if (prime(n) == 1){
										System.out.println(n+" is a prime number.");
									}
									else if (primeAnswer == 0){
										System.out.println(n+" is not a prime number.");
									}
							}
						//This will return you to the menu
								else{
									System.out.println(error);
								}
							System.out.println(" ");
								break;
						case 3:
							System.out.println("Input a number between 2 and 20:");
							n = keyboard.nextInt();
							//Restricts input to 2-20
								if(n>=2 && n <= 20){
									int factorial = getFactorial(n);									
									System.out.println("Factorial of "+n+" = "+ factorial);
									}
							// Returns you to menu
								else{
									System.out.println(error);
								}
								System.out.println(" ");
								break;
						case 4:
							System.out.println("Input a number greater than or equal to 5:");
							n = keyboard.nextInt();
							//Restricts inputs to only greater than 5
								if(n>=5){
									System.out.print("Fibonacci sequence: ");
									getFibonacci(n);
									System.out.print("\b\b.");
									System.out.println(" ");
								}
							//Returns to menu
								else{
									System.out.println(error);
								}
							System.out.println(" ");
								break;
						case 5:
							System.out.println("Input a number greater than 1:");
							n = keyboard.nextInt();
							//if n does not match criteria then the program will return to menu
								if(n >= 1){
									System.out.println("Input a number greater than 0:");
									d = keyboard.nextInt();
								//If d does not match criteria then the program will return to menu
										if(d<=n){
											boolean divisibility =divisible(n,d);
										//If return from method is true then print this statement
											if(divisibility != true){
												System.out.println(n+" is not divisible by "+d+".");
											}
										//False would print this statement
											else{
												System.out.println(n+" is divisible by "+d+".");
											}
										}
									//Return to menu
										else{
										System.out.println(error);
										}
								}
							//Return to menu
								else{
								System.out.println(error);
									}
							System.out.println(" ");
								break;
						case 0:
						//Ends the loop
							break;
						default:
							System.out.println(error);
							break;
					}
				//End of do-while loop
				}while(choice != 0);
			System.out.println(farewell);
	}
	//MENU METHOD
	public static void menu(String mainMenu){
		mainMenu="Menu of Math Operations: \n1-Factors of a number. \n2-Prime number or not. \n3-Factorial of a number. \n4-Fibonacci sequence. \n5-Number evenly divides another number. \n0-End of program. \nInput your choice:";
		System.out.println(mainMenu);
	}
	//FACTOR METHOD
	public static void getFactor(int newN){
		int i=1;
		System.out.print("\nFactors of "+newN+" are: ");
		while(i <= newN){
				if(newN % i == 0){
					if(i!=newN){
					System.out.print(i+", ");
					}
					else{
						System.out.print(i+".");
					}
					}
			i++;
		}
		System.out.print("\n");
	}	
	//PRIME FUNCTION
	public static int prime(int nPrime){
	//i begins at 2 because every prime number is divisible by 1 and would make
	//nPrime = 0 everytime
		for (int i=2;i<= nPrime/2;++i){
			if (nPrime%i==0){
				//if the remainder of nPrime and i is 0 
				//then it is not prime and will return 0 
				return nPrime=0; 
				}
			// if it is prime then it will return 1
		}
			return nPrime=1;
	}
	//FACTORIAL FUNCTION
	public static int getFactorial(int factorialN){
		//New variable to calculate factorial
		int fact = 1;
		for(int i =1; i<= factorialN; ++i){
			//the factorial function. 
			fact = fact * i;
		}
			return fact;
	}
	//FIBONACCI METHOD
	public static void getFibonacci(int newN){
		int i=0, x = 1, xPrev = 0;
		//Prints out 0 and 1 as first in sequence
		System.out.print(xPrev+", "+x+", ");
			//To enter 0 and 1 within the print statement, 
			//I decreased the new N by 2 and physically printed 0 and 1 prior
			for(i = 0; i < newN-2; ++i){
			//Save current x to a different variable labeled ogX
			  int ogX = x;
			//Add x and previous x to get new x
			  x = x+xPrev;
			//Previous x then becomes the original x labeled ogX
			  xPrev = ogX;
			//Then x is printed out
			  System.out.print(x+", ");
			 }
			 
		}
	//EVEN DIVISIBILITY FUNCTION
	public static boolean divisible(int numOne, int numTwo){
		//Returns a true or false
		boolean divisibility;
		if(numOne%numTwo == 0){
			//If remainder is 0 return true
			divisibility = true;
			return divisibility;
		}
		//Otherwise it is false
		else{
			divisibility = false;
			return divisibility;
		}
		}
}
